* have ssh access into the server without password (follow https://gitlab.com/gcelano/install_ssh/-/blob/main/instructions.md)
* Install SSHFS:
  * if the client has Ubuntu: `sudo apt update` and `sudo apt install sshfs`
  * if the client has MacOS: install MACFUSE https://osxfuse.github.io/ and then SSHFS https://github.com/libfuse/sshfs/releases
* create a directory `mkdir ~/mount`
* connect `sshfs technician@ubuntu:/home/technician /Users/technician/mount`
* files in `~/mount` can now be accessed as if they were local (i.e., using the local Apps)
* to close the connection: `umount ~/mount`
